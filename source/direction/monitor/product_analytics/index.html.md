---
layout: markdown_page
title: "Category Direction - Product Analytics"
description: "Product analytics tools tell you how, what, when, where, and the who."
canonical_path: "/direction/monitor/product_analytics/"
---

- TOC
{:toc}
## Welcome
This category is primarily the responsibility of the Product Intelligence Group, and is maintained by [Amanda Rueda](https://gitlab.com/amandarueda). This direction is a work in progress and everyone can contribute. Sharing your feedback directly on issues and epics at GitLab.com is the best way to contribute. If you’re a GitLab user and have direct knowledge of your need for product analytics, we’d especially love to hear from you.

## Overview
Understanding how your users use your product, what they do with your product, when they use your product, where they are interacting with your product, who they are, and what problems they run into, are important feedback that informs future improvements. That is the essence of Product Analytics. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/TyLoP08GaMU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
<!-- Presentation Source: https://docs.google.com/presentation/d/1fFwyrW6UCK_-fnIncfanuj1hIdbWWLZRsKBixLwGVzA/edit#slide=id.gfd09877cb5_0_48 -->

## Vision
GitLab will continue to define the most complete DevOps platform by including Product Analytics as part of the critical business feedback loop. 

## Challanges
- **We’ve added limited investment in this category** - As a result our current capabilities are non-usable. 
- **We lack a purpose built data-store for product analytics data** - Product analytics data requires a data-store that is designed for high-ingest and analysis. Our current product analytics capability is backed by Postgres.
- **Existing vendors invest heavily in integrations** - there are no common standards for data ingest and export. As a result, many tools have created unique libraries of integrations to bring data into and ship data out of their platforms.
- **We have an immediate business need for a tool internally** - Combined with our limited investment means we’ll select a third party tool instead of dogfooding.

## Opportunities
- **Existing tools struggle with developers** - Current CDP and Product Analytics tools are built for marketing and product teams. Before they can be useful for those teams though developers must implement, understand and instrument their applications for them.
- **Low market penetration of product analytics tools** - There are a few large existing players - Pendo, MixPanel, Amplitude, Segment - but [market penetration and use of such tools remains low](https://www.prnewswire.com/news-releases/global-product-analytics-market-2021-to-2027---by-component-vertical-enterprise-size-deployment-type-end-user-and-region-301368415.html).
- **Fantastic experience benefits from single-platform** - There are countless benefits of an integrated Product Analytics capability with our DevOps platform including usage behavior context while writing code, business metric tracking alongside deployments including feature flag roll-out, improved value stream mapping, and consolidated feedback across issues, service-desk and usage to aid in prioritization.
- **There are open-source tools we can learn from** - There are existing open-source tools (like [PostHog](https://posthog.com/)) that have created Product Analytics platforms that provide insight into the market need and technical challenges.

## Strategic Objectives
Given our challenges and opportunities there are a clear set of strategic objectives we need to maintain:
- **Target developers first** - This allows us to utilize our dual-flywheel model to continue to expand our platform.
- **Build for future adopters** - There are more future adopters than current. A complete solution, which targets them without investing heavily in integrations to start, is prudent. 
- **Build on ClickHouse** - As an example, [Posthog has recently migrated to ClickHouse](https://posthog.com/blog/clickhouse-announcement) and GitLab has already selected [ClickHouse](https://clickhouse.com/) as a future technology for similar data.
- **Participate in Open-source Tools and Standards** - We should continue to learn, leverage and contribute to existing open-source tools and standards including [instrumentation libraries and integrations](https://posthog.com/docs/integrate).
- **Ensure a migration path** - While we will select an internal tool for our immediate needs we should ensure we have a migration path to use our Product Analytics capabilities in the future.

## Target Audience and Experience

As part of the strategic objectives above we'll first target developers. Here's why:
- It's unlikely that we'll be able to immediately replace large Product Analytics platforms for organizations that have entire Product Management and Product Analytics departments
- In order to start our [dual flywheel](/company/strategy/#dual-flywheels) we should target [developers first](/handbook/product/product-principles/#developer-first)
- Existing teams struggle to instrument even enterprise-grade product analytics tools
- There is substantial value we can provide for a small development team that would otherwise choose NOT to invest in a Product Analytics platform

Focusing on developers will allow us to start our dual flywheel and gain a foothold in this critical market.

## Plan
Given these strategic objectives, our plan is to:

- Build product analytics capabilities on top of ClickHouse going forward
- Weight our selection criteria for an internal tool based on the strategic objectives (developers focus, future proof, support for ClickHouse, contributability, migratable)
- Document and maintain current migration plan as our internal tool selection and investment level progresses

## Dogfooding Migration Plan
Below is our draft migration plan to move from our implementation of a third-party Product Analytics platform to dogfooding our own growing Product Analytics capabilities.

![Product Analytics Migration Plan](Product-Analytics-Migration.png)
<!-- Source: https://docs.google.com/drawings/d/1UmAgS4lmmNZPwlr3CHqhvmfMlTnGB7NwMFBtp6-zcQ4/edit -->

<!--
## Dogfooding Plan

## Competitive Landscape

## Analyst Landscape
-->
